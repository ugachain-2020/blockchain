package com.uga.blockchain.domain.enumeration;

/**
 * The RequestType enumeration.
 */
public enum RequestType {
    ADDITION, DELETION, SET, GET
}
