/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uga.blockchain.web.rest.vm;
