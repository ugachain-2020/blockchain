import { Moment } from 'moment';

export const enum RequestType {
    ADDITION = 'ADDITION',
    DELETION = 'DELETION',
    SET = 'SET',
    GET = 'GET'
}

export interface IRequest {
    id?: number;
    type?: RequestType;
    date?: Moment;
    key?: string;
    value?: string;
    status?: string;
}

export class Request implements IRequest {
    constructor(
        public id?: number,
        public type?: RequestType,
        public date?: Moment,
        public key?: string,
        public value?: string,
        public status?: string
    ) {}
}
