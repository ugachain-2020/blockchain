import { NgModule } from '@angular/core';

import { BlockchainSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [BlockchainSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [BlockchainSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class BlockchainSharedCommonModule {}
